import Client.PostUserDetailsClient.PostRequest.PostUserDetailsRequest;
import Client.PostUserDetailsClient.PostResponse.PostUserDetailsResponse;
import Client.PostUserDetailsClient.PostUserDetails;
import org.testng.Assert;
import org.testng.annotations.Test;



public class POSTusingTestNg {
    @Test
    public void gettingFirstPutApi() {

        //Data
        PostUserDetailsRequest PostRequest = new PostUserDetailsRequest();
        PostRequest.setId(4);
        PostRequest.setTitle("Kame");
        PostRequest.setCompleted(true);
        PostRequest.setDueDate("2021-08-03T16:25:37.974Z");

        //Getting response
       PostUserDetailsResponse postUserDetailsResponse = new PostUserDetails()
               .postuserdetails(PostRequest,"https://fakerestapi.azurewebsites.net/api/v1/Activities");

       // Assert.assertTrue(postUserDetailsResponse.getTitle().equals("Kame"));
        System.out.println(postUserDetailsResponse.getTitle());

    }
    }

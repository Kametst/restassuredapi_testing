import Client.GetBPopulartshirtClinet.GetPopularTshirt;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class GETUsingTestNg {

    @Test
    public void GetTestMethod(){
       int StatusCode = new GetPopularTshirt()
                .getPopularTshirt("https://www.bewakoof.com/men-clothing/default",
                        "popular");
        Assert.assertTrue(StatusCode==200);
    }
}

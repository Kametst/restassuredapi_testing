package MakeUpApiAutomation;

import Client.MakeUpPageClient.NaturalProductClient.NaturalProductDetails;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class NaturalProductApi {
    @Test
    public void naturalProductApi() throws ParseException, IOException {

        NaturalProductDetails naturalProductDetails = new NaturalProductDetails();
        int statusCode = naturalProductDetails.findNaturalProduct("dev");


        Assert.assertTrue(statusCode==200);
    }
}

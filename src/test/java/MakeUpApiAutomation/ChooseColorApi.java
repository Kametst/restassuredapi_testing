package MakeUpApiAutomation;
import Client.MakeUpPageClient.ChooseColorClient.ChooseColor;
import Utilities.PropertyReader;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Properties;


public class ChooseColorApi {

    @Test
    public void chooseColorApi() throws ParseException, IOException {

        ChooseColor Black = new ChooseColor();
        //sending "dev" to PropertyReader file it will add this "dev"
        // with the "env.properties" i.e devenv.properties
        int statusCode = Black.blackcolor("dev");

         Assert.assertTrue(statusCode==200);

    }

}

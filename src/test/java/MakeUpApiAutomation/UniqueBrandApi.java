package MakeUpApiAutomation;
import Client.MakeUpPageClient.UniqueBrandClient.UniqueBrandDetails;
import io.restassured.response.Response;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import static io.restassured.RestAssured.given;

public class UniqueBrandApi {
    @Test
    public void uniqueBrandApi() throws ParseException, IOException {
        UniqueBrandDetails uniqueBrandDetails = new UniqueBrandDetails();
        int statusCode = uniqueBrandDetails.uniqueBrand("dev");

        Assert.assertTrue(statusCode==200);
    }
}

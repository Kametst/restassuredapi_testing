import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class QueryPramApiTest {
    @Test
    public void QueryMethod(){

        //GET request using Parameter ID and using content type as JSON
        Response userData = given()
                .contentType(ContentType.JSON)
                .param("id" , 1)
                .when()
                .get("https://reqres.in/api/users?page=2&id=12");
        System.out.println(userData.body().asString());
    }

}

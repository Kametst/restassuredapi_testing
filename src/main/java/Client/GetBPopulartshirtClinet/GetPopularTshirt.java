package Client.GetBPopulartshirtClinet;


import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GetPopularTshirt {
    public int getPopularTshirt(String popularTshirtApi, String query) {

        //URL : "https://www.bewakoof.com/men-clothing/default?sort=popular"

        Response popularTshirtCollection= given()
                .queryParam("sort",query)
                .when()
                .get(popularTshirtApi);
        return popularTshirtCollection.statusCode();
    }
}

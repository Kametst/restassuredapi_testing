package Client.OpenWeatherMapClient.GetWeatherReportClient;

import Utilities.PropertyReader;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class GetLocationWeatherDetails {
    public int getLocationWeather(String env) throws ParseException, IOException {
        Properties properties = new PropertyReader().propertyReader(env);
        Response response = given()
                .when()
                .headers("x-rapidapi-key", properties.getProperty("headersKey"))
                .headers("x-rapidapi-host", properties.getProperty("headersHost"))
                .get(properties.getProperty("weatherBasePath")+properties.getProperty("locationPath"));

        JSONParser p = new JSONParser();
        Object obj = p.parse(response.asString());
        JSONObject jObj = (JSONObject) obj;
        JSONArray listArray = (JSONArray) jObj.get("list");

        JSONObject list = (JSONObject) listArray.get(0);
        JSONArray weatherArray = (JSONArray) list.get("weather");

        JSONObject weather = (JSONObject) weatherArray.get(0);
        System.out.println(weather.get("description"));

        return response.statusCode();
    }
}

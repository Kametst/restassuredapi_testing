package Client.PostUserDetailsClient;
import Client.PostUserDetailsClient.PostRequest.PostUserDetailsRequest;
import Client.PostUserDetailsClient.PostResponse.PostUserDetailsResponse;
import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


import static io.restassured.RestAssured.given;

public class PostUserDetails {
    public PostUserDetailsResponse postuserdetails(PostUserDetailsRequest postUserDetailsRequest, String PostDetailAPI) {

        Response userData = given()
                .when()
                .contentType(ContentType.JSON)
                .body(new Gson().toJson(postUserDetailsRequest))
                .post(PostDetailAPI);

        return new Gson().
                fromJson(userData.body().asString(), PostUserDetailsResponse.class);

    }

}

package Client.PostUserDetailsClient.PostResponse;

public class PostUserDetailsResponse {
    public int id;
    public String title;
    public String dueDate;
    public boolean completed;

    public String getDueDate() {
        return dueDate;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }



    public boolean isCompleted() {
        return completed;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}

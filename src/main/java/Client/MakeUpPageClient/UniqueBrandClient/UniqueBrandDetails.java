package Client.MakeUpPageClient.UniqueBrandClient;

import Utilities.PropertyReader;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;

import static io.restassured.RestAssured.given;

public class UniqueBrandDetails {
    public int uniqueBrand(String env) throws ParseException, IOException {
        Properties properties = new PropertyReader().propertyReader(env);
        Response response = given()
                .when()
                .get(properties.getProperty("makeuBasePath")+properties.getProperty("getProduct"));


        JSONParser p= new JSONParser();
        Object obj=p.parse(response.asString());
        JSONArray listArray=(JSONArray)obj;
        ArrayList<String> brand=new ArrayList<>();

        for (int i=0;i<listArray.size();i++){
            JSONObject list = (JSONObject) listArray.get(i);
            String brandName= (String) list.get("brand");
            brand.add(brandName);
        }
        Set<String> uniqueBrand = new LinkedHashSet<>(brand);
        System.out.println("Unique Brand are:");

        for (String b:uniqueBrand) {
            System.out.println(b);
        }
        return response.statusCode();
    }
}

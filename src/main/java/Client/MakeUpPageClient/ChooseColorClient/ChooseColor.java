package Client.MakeUpPageClient.ChooseColorClient;

import Utilities.PropertyReader;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class ChooseColor {
    public int blackcolor(String env) throws ParseException, IOException {

        //using property files to get the base path and product path
        Properties properties = new PropertyReader().propertyReader(env);
        Response response = given()
                .when()
                .get(properties.getProperty("makeuBasePath")+properties.getProperty("getProduct"));


        JSONParser parsejson = new JSONParser();
        Object obj = parsejson.parse(response.asString());
        JSONArray listArray = (JSONArray) obj;
        ArrayList<String> blckcolor = new ArrayList<>();

        for (int i = 0; i < listArray.size(); i++) {
            JSONObject list = (JSONObject) listArray.get(i);
            JSONArray colorarray = (JSONArray) list.get("product_colors");
            for (int j = 0; j < colorarray.size(); j++) {
                JSONObject colorobj = (JSONObject) colorarray.get(j);
                String color = (String) colorobj.get("colour_name");
                if (color != null) {
                    if (color.equalsIgnoreCase("black")) {
                        String ItemName = (String) list.get("name");
                        blckcolor.add(ItemName);
                    }
                }

            }
        }

        for (String b:blckcolor) {
            System.out.println(b);
        }
        return response.statusCode();
    }
}

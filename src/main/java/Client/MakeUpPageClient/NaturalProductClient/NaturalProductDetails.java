package Client.MakeUpPageClient.NaturalProductClient;

import Utilities.PropertyReader;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class NaturalProductDetails {
    public int findNaturalProduct(String env) throws ParseException, IOException {

        Properties properties = new PropertyReader().propertyReader(env);
        Response response = given()
                .when()
                .get(properties.getProperty("makeuBasePath")+properties.getProperty("getProduct"));

        JSONParser parsejson = new JSONParser();
        Object obj = parsejson.parse(response.asString());
        JSONArray listArray = (JSONArray) obj;
        ArrayList<String> brand = new ArrayList<>();

        for (int i = 0; i < listArray.size(); i++) {
            JSONObject list = (JSONObject) listArray.get(i);
            String prodctdiscription = (String) list.get("description");
            if(prodctdiscription!=null){
                if (prodctdiscription.contains("natural")) {
                    String ItemName = (String) list.get("name");
                    brand.add(ItemName);
                }
            }
        }
        System.out.println("Unique Brands are:");
        for (String b : brand) {
            System.out.println(b);
        }
        return response.statusCode();
    }
}
